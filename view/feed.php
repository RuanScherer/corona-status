<?php

    session_start();

    include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/controller/PostController.php");

    if (!isset($_SESSION['id']))
    {
        Header("Location: login.php");
    }

    if (isset($_GET['logout'])) {
        session_destroy();
        Header("Location: ../index.php");
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Corona Status</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="../styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content radius shadow">
                    <div class="modal-header">
                        <h5 class="modal-title text-dark" id="postModalLabel">Novo post</h5>
                    </div>
                    <div class="modal-body">
                        <form id="postForm" action="?publish" method="post">
                            <div class="form-group">
                                <textarea type="text" class="form-control input-radius" id="content" name="content" placeholder="Diga algo..." rows="3" required></textarea>
                            </div>
                            <div class="form-group">
                                <select class="custom-select input-radius" name="feeling" required>
                                    <option value="bem" selected>Me sinto bem</option>
                                    <option value="com suspeita">Estou com suspeita</option>
                                    <option value="mal">Me sinto mal</option>
                                </select>
                            </div>

                            <?php

                            if (isset($_GET['publish']))
                            {
                                $postController = new PostController($_POST);

                                if ($postController->create())
                                {
                                    Header("Location: feed.php");
                                }
                                else
                                {
                                    echo "
                                        <p class='alert alert-danger alert-dismissible input-radius fade show' role='alert'>
                                          Erro ao publicar, tente novamente.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </p>
                                    ";
                                }
                            }

                            ?>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link text-decoration-none text-danger input-radius" data-dismiss="modal">Descartar</button>
                        <button id="publish" class="btn btn-custom input-radius">
                            <span class="spinner-grow spinner-grow-sm d-none" id="spinner" role="status" aria-hidden="true"></span>
                            Publicar
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="app">
            <nav class="navbar navbar-expand-md navbar-dark bg shadow-sm">
                <div class="container">
                    <a href="feed.php" class="navbar-brand">
                        Corona Status
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="profile.php?current"><?php echo $_SESSION['name'] ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?logout">Sair</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                <div class="container my-2 d-flex flex-column align-items-center">
                    <div class="btn-group w-100 mb-3 input-radius" role="group">
                        <a href="feed.php" class="btn btn-outline btn-outline-custom color input-radius border-custom d-flex align-items-center justify-content-center">
                            <svg class="bi bi-arrow-repeat mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" d="M2.854 7.146a.5.5 0 00-.708 0l-2 2a.5.5 0 10.708.708L2.5 8.207l1.646 1.647a.5.5 0 00.708-.708l-2-2zm13-1a.5.5 0 00-.708 0L13.5 7.793l-1.646-1.647a.5.5 0 00-.708.708l2 2a.5.5 0 00.708 0l2-2a.5.5 0 000-.708z" clip-rule="evenodd"/>
                              <path fill-rule="evenodd" d="M8 3a4.995 4.995 0 00-4.192 2.273.5.5 0 01-.837-.546A6 6 0 0114 8a.5.5 0 01-1.001 0 5 5 0 00-5-5zM2.5 7.5A.5.5 0 013 8a5 5 0 009.192 2.727.5.5 0 11.837.546A6 6 0 012 8a.5.5 0 01.501-.5z" clip-rule="evenodd"/>
                            </svg>
                            Atualizar
                        </a>
                        <button type="button" class="btn btn-outline btn-outline-custom color input-radius border-custom d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#postModal">
                            <svg class="bi bi-pencil-square mr-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                              <path d="M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z"/>
                              <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z" clip-rule="evenodd"/>
                            </svg>
                            Publicar
                        </button>
                    </div>
                    
                    <?php

                    $postController = new PostController($_POST);

                    $posts = $postController->list();

                    if($posts != false)
                    {
                        while ($row = mysqli_fetch_assoc($posts)) {
                            echo "
                                <div class='card w-100 radius border-";

                            switch ($row['feeling']) {
                                case 'bem':
                                    echo "good";
                                    break;

                                case 'com suspeita':
                                    echo "medium";
                                    break;

                                case 'mal':
                                    echo "bad";
                                    break;
                            }

                            echo " mb-3 shadow-sm'>
                                    <div class='card-body'>";

                            if($row['id_user'] == $_SESSION['id'])
                            {
                                echo "
                                    <a class='h4 text-decoration-none text-dark' href='profile.php?current'>".$row['name']."</a>
                                ";
                            }
                            else
                            {
                                echo "
                                    <a class='h4 text-decoration-none text-dark' href='profile.php?id=".$row['id_user']."'>".$row['name']."</a>
                                ";
                            }
                            
                            echo "            
                                        <p class='card-text h6 card-text font-weight-light text-dark py-1'>".$row['content']."</p>
                                        <div class='dropdown-divider'></div>
                                        <p class='card-text h6 font-weight-light text-muted'>".date("d/m/Y H:i:s", strtotime($row['published_at']))."</p>
                                    </div>
                                </div>
                            ";
                        }
                        echo "
                            <span class='mb-3 text-muted h6 align-self-center'>Os posts acabam aqui.</span>
                        ";
                    }
                    else
                    {
                        echo "
                            <div class='card radius border-default mb-3 shadow-sm'>
                                <div class='card-body'>
                                    <a class='h4 text-decoration-none text-secondary' href='#'>Oh não! Ainda não há nada aqui.</a>
                                    <p class='card-text h6 card-text font-weight-regular text-muted py-1'>Publique para incetivar as pessoas ao seu redor!</p>
                                </div>
                            </div>
                        ";
                    }

                    ?>
                </div>
            </main>

            <button id="backToTop" class="d-none fixed-bottom btn w-100 btn-custom shadow-lg input-radius py-2">Ir para o topo</button>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
            document.querySelector("#publish").onclick = (evt) => {
                evt.target.innerHTML = "Publicando...";
                $("#spinner").removeClass("d-none");
                document.querySelector("#postForm").submit();
            }

            let height = window.innerHeight / 2;

            $(window).on('scroll', function() {
                if($(window).scrollTop() >= height) { 
                    $('#backToTop').removeClass('d-none');
                    $('#backToTop').removeClass('fade-out');
                    $('#backToTop').addClass('fade-up');
                }
                else {
                    $('#backToTop').removeClass('fade-up');
                    $('#backToTop').addClass('fade-out');
                    setTimeout(() => {
                        $('#backToTop').addClass('d-none');
                    }, 500);
                }
            });
        </script>
    </body>
</html>