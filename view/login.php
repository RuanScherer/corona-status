<?php

    session_start();

    include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/controller/UserController.php");

    if (isset($_SESSION['id']))
    {
        Header("Location: feed.php");
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Corona Status</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="../bootstrap/bootstrap.css">
        <link rel="stylesheet" href="../styles.css">

    </head>
    <body>
        <main class="w-100 p-3 bg-linear text-dark text-center d-flex flex-column align-items-center justify-content-center mh-100">
            <div class="w-100 p-3" style="max-width: 650px">
                <div class="card radius shadow w-100 p-md-3 p-2">
                    <div class="card-body">
                        <h3 class="card-subtitle color">Corona Status</h3>
                        <form class="mt-4" action="?login" method="POST">
                            <div class="form-group">
                                <input type="email" class="form-control input-radius text-center" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control input-radius text-center" id="password" name="password" placeholder="Senha">
                            </div>

                            <?php

                            if (isset($_GET['login']))
                            {
                                $userController = new UserController($_POST);

                                if ($userController->login())
                                {
                                    Header("Location: feed.php");
                                }
                                else
                                {
                                    echo "
                                        <p class='alert alert-danger input-radius fade show p-2' role='alert'>
                                          Erro ao logar, tente novamente.
                                        </p>
                                    ";
                                }
                            }

                            ?>

                            <button class="btn btn-outline btn-custom btn-block input-radius my-3">Entrar</button>
                            <a href="register.php" class="text-decoration-none text-muted">Não tem uma conta? Cadastre-se.</a>
                        </form>
                    </div>
                </div>
            </div>
        </main>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>

