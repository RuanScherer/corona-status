<?php

    session_start();

    include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/controller/PostController.php");
    include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/controller/UserController.php");

    if (!isset($_SESSION['id']))
    {
        Header("Location: login.php");
    }

    if (isset($_GET['logout'])) {
        session_destroy();
        Header("Location: ../index.php");
    }

    $userController = null;
    $postController = null;

    if(isset($_GET['current']))
    {
        $userController = new UserController($_SESSION);
        $postController = new PostController($_SESSION);
    }
    elseif(isset($_GET['id'])) {
        $userController = new UserController($_GET);
        $postController = new PostController($_GET);
    }
    else
    {
        Header("Location: feed.php");
    }

    if (isset($_GET['delete'])) {
        $_GET['id'] = $_GET['delete'];
        $postController = new PostController($_GET);
        $postController->delete();
        Header("Location: profile.php?current");
    }

    $user = $userController->get();
    $posts = $postController->listByUser();

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Corona Status</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="../bootstrap/bootstrap.css" rel="stylesheet">
        <link href="../styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="deleteModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content radius shadow">
                    <div class="modal-header">
                        <h5 class="modal-title text-danger">Ação importante</h5>
                    </div>
                    <div class="modal-body">
                        <p class="card-text">Tem certeza que deseja excluir o post?</p>
                    </div>
                    <div class="modal-footer p-2">
                        <button type="button" class="btn btn-link-secondary" data-dismiss="modal">Não.</button>
                        <a href="?current&delete=" id="confirmDelete" class="btn btn-danger input-radius">Sim, excluir.</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-dark bg shadow-sm">
                <div class="container">
                    <a href="feed.php" class="navbar-brand">
                        Corona Status
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="profile.php?current"><?php echo $_SESSION['name'] ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="?logout">Sair</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                <div class="w-100 d-flex flex-wrap justify-content-center">
                    <div class="col-md-3 w-100 my-2">
                        <?php
                            if(isset($_GET['current']))
                            {
                        ?>
                        <div class="card radius shadow-sm">
                            <div class="card-body">
                                <h4 class="text-dark">Seu perfil</h4>
                                <div class="dropdown-divider" ></div>
                                <form id="edit-form" action="?edit&current" method="post" class="mt-3">
                                    <?php 
                                    
                                    while ($row = mysqli_fetch_assoc($user)) 
                                    {
                                    
                                    ?>
                                    <div class="form-group">
                                        <label for="name">Nome</label>
                                        <input type="text" class="form-control input-radius" id="name" name="name" placeholder="Nome completo" value="<?php echo $row['name']; ?>" disabled required></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control input-radius" id="email" name="email" placeholder="Email" value="<?php echo $row['email']; ?>" disabled required></input>
                                    </div>

                                    <?php
                                    
                                    }

                                    if (isset($_GET['edit']))
                                    {
                                        $userController = new UserController($_POST);

                                        if ($userController->edit())
                                        {
                                            Header("Location: profile.php?current");
                                        }
                                        else
                                        {
                                            echo "<script>alert('caiu no edit')</script>";
                                            echo "
                                                <p class='alert alert-danger input-radius fade show text-center p-2' role='alert'>
                                                  Erro ao salvar as informações, tente novamente.
                                                </p>
                                            ";
                                        }
                                    }

                                    ?>

                                    <button id="edit" class="btn btn-outline btn-custom btn-block input-radius mt-3">
                                        <span class="spinner-grow spinner-grow-sm d-none" id="spinner" role="status" aria-hidden="true"></span>
                                        Editar
                                    </button>
                                    <button id="cancel" class="btn btn-outline btn-outline-danger btn-block input-radius d-none mt-3">Cancelar</button>
                                    <a href="#" id="reset-password" class="btn btn-outline btn-outline-danger btn-block input-radius mt-3">Alterar senha</a>
                                </form>
                            </div>
                        </div>

                        <?php
                            }
                            else
                            {
                                while ($row = mysqli_fetch_assoc($user)) 
                                {
                        ?>

                        <div class="card radius shadow-sm">
                            <div class="card-body">
                                <h4 class="text-dark"><?php echo $row['name']; ?></h4>
                                <span class="h5"><?php echo $row['email']; ?></span>
                            </div>
                        </div>

                        <?php
                                } 
                            }
                        ?>

                    </div>

                    <div class="col-md-8 w-100 my-2 d-flex flex-column align-items-center">
                        <h3 class="align-self-start text-dark">Posts do usuário</h3>
                        <?php

                        if($posts != false)
                        {
                            while ($row = mysqli_fetch_assoc($posts)) {
                                echo "
                                    <div class='card w-100 radius border-";

                                switch ($row['feeling']) {
                                    case 'bem':
                                        echo "good";
                                        break;

                                    case 'com suspeita':
                                        echo "medium";
                                        break;

                                    case 'mal':
                                        echo "bad";
                                        break;
                                }

                                echo " mb-3 shadow-sm'>
                                        <div class='card-body'>
                                            <span class='h4 text-decoration-none text-dark'>".$row['name']."</span>
                                            <p class='card-text h6 card-text font-weight-light text-dark py-1'>".$row['content']."</p>
                                            <div class='dropdown-divider'></div>
                                            <div class='d-flex align-items-center justify-content-between'>
                                                <p class='card-text h6 text-muted font-weight-light'>".date("d/m/Y H:i:s", strtotime($row['published_at']))."</p>";
                                if(isset($_GET['current']))
                                {
                                    echo "
                                        <svg class='bi bi-trash text-danger remove' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg' onclick='remove(".$row['id_post'].")'>
                                          <path d='M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z'/>
                                          <path fill-rule='evenodd' d='M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z' clip-rule='evenodd'/>
                                        </svg>
                                    ";
                                }
                                                
                                echo "      </div>
                                        </div>
                                    </div>
                                ";
                            }
                            echo "
                                <span class='mb-3 text-muted h6 align-self-center'>Os posts acabam aqui.</span>
                            ";
                        }
                        else
                        {
                        ?>
                            <div class='card radius border-default mb-3 shadow-sm'>
                                <div class='card-body'>
                                    <a class='h4 text-decoration-none text-secondary' href='#'>Oh não! Ainda não há nada aqui.</a>
                                    <p class='card-text h6 card-text font-weight-regular text-muted py-1'>Publique para incetivar as pessoas ao seu redor!</p>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </main>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
            let editButton = document.querySelector("#edit");
            let nameInput = document.querySelector("#name");
            let emailInput = document.querySelector("#email");
            let cancelButton = document.querySelector("#cancel");
            let resetPasswordButton = document.querySelector("#reset-password");
            let spinner = document.querySelector("#spinner");

            let formStatus = false;

            editButton.onclick = (evt) => {
                evt.preventDefault();
                if(!formStatus) {
                    evt.target.innerHTML = "Salvar";
                    nameInput.disabled = false;
                    emailInput.disabled = false;
                    cancelButton.classList.remove("d-none");
                    resetPasswordButton.classList.add("d-none");
                    nameInput.select();
                    formStatus = true;
                } else {
                    evt.target.innerHTML = "Salvando..."
                    document.querySelector("#edit-form").submit();
                }
            }

            cancelButton.onclick = (evt) => {
                evt.preventDefault();
                spinner.classList.remove("d-none");
                editButton.innerHTML = "Editar";
                nameInput.disabled = true;
                emailInput.disabled = true;
                cancelButton.classList.add("d-none");
                resetPasswordButton.classList.remove("d-none");
                formStatus = false;
            }

            function remove(postId) {
                document.querySelector("#confirmDelete").href += postId;
                $('#deleteModal').modal('show')
            }
        </script>
    </body>
</html>