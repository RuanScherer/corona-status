<?php 

include_once("Connection.php");

class User extends Connection
{
	protected $id;
	protected $name;
	protected $email;
	protected $password;
	
	public function __construct($props)
	{
		if(isset($props['id']))
		{
			$this->id = $props['id'];
		}
		if(isset($props['name']))
		{
			$this->name = $props['name'];
		}
		if(isset($props['email']))
		{
			$this->email = $props['email'];
		}
		if(isset($props['password']))
		{
			$this->password = $props['password'];
		}
	}

	public function connect()
	{
		return $this->getConnection();
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

}

?>