<?php 

include_once("Connection.php");

class Post extends Connection
{
	protected $id;
	protected $content;
	protected $feeling;
	protected $user_id;
	
	public function __construct($props)
	{
		if(isset($props['id']))
		{
			$this->id = $props['id'];
		}
		if(isset($props['content']))
		{
			$this->content = $props['content'];
		}
		if(isset($props['feeling']))
		{
			$this->feeling = $props['feeling'];
		}
		if(isset($props['user_id']))
		{
			$this->user_id = $props['user_id'];
		}
	}

	public function connect()
	{
		return $this->getConnection();
	}

	public function __get($prop)
	{
		return $this->$prop;
	}

	public function __set($prop, $value)
	{
		$this->prop = $value;
	}

}

?>