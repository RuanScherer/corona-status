<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/model/Post.php");

class PostController extends Post
{

	public function __construct($props)
	{
		parent::__construct($props);
	}

	public function create()
	{
		$query = "insert into post values(null, '".$this->content."', '".$this->feeling."', '".$_SESSION['id']."', now())";

		if(mysqli_query($this->connect(), $query))
		{
			return true;
		}
		return false;
	}

	public function list()
	{
		$query = "select post.*, id_user, name from post inner join user on id_user = fk_user order by published_at desc";
		$response = mysqli_query($this->connect(), $query);

		if(mysqli_num_rows($response) >= 1)
		{
			return $response;
		}
		return false;
	}

	public function listByUser()
	{
		$query = "select post.*, name from post inner join user on id_user = fk_user where fk_user = ".$this->id." order by published_at desc";
		$response = mysqli_query($this->connect(), $query);

		if(mysqli_num_rows($response) >= 1)
		{
			return $response;
		}
		return false;
	}

	public function delete()
	{
		$userConfirm = mysqli_query($this->connect(), "select fk_user from post where id_post = ".$this->id);
		while ($row = mysqli_fetch_assoc($userConfirm))
		{
			if($row['fk_user'] == $_SESSION['id'])
			{
				if (mysqli_query($this->connect(), "delete from post where id_post = ".$this->id)) 
				{
					return true;
				}
				return false;
			}
			return false;
		}
	}

}

?>