<?php 

include_once($_SERVER['DOCUMENT_ROOT']."/corona-status/model/User.php");

class UserController extends User
{

	public function __construct($props)
	{
		parent::__construct($props);
	}

	public function login()
	{
		$query = "select * from user where email = '".$this->email."'";
		$response = mysqli_query($this->connect(), $query);

		if(mysqli_num_rows($response) >= 1)
		{
			while($row = mysqli_fetch_assoc($response))
			{
				if(password_verify($this->password, $row['password']))
				{

					$_SESSION['id'] = $row['id_user'];
					$_SESSION['name'] = $row['name'];
					$_SESSION['email'] = $row['email'];
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			session_destroy();
			return false;
		}
	}

	public function register()
	{
		$query = "insert into user values(null, '".$this->name."', '".$this->email."', '".password_hash($this->password, PASSWORD_DEFAULT)."')";
		if(mysqli_query($this->connect(), $query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get()
	{
		$query = "select * from user where id_user = '".$this->id."'";
		$response = mysqli_query($this->connect(), $query);
		if($response)
		{
			return $response;
		}
		else
		{
			return false;
		}
	}

	public function edit()
	{
		$query = "update user set name = '".$this->name."', email = '".$this->email."' where id_user = '".$_SESSION['id']."'";
		if(mysqli_query($this->connect(), $query))
		{
			$_SESSION['name'] = $this->name;
			$_SESSION['email'] = $this->email;
			return true;
		}
		else
		{
			return false;
		}
	}

}

?>